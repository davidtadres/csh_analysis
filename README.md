# CSH_analysis

Contains a few scripts to run analysis as part of the CSH Drosphila Neurobiology summer school

To install on CSH laptops:

1) Install either Miniconda (https://conda.io/miniconda.html) or
Anaconda (https://www.anaconda.com/download/).

2) Install Git (https://git-scm.com/downloads) (already installed on
many Linux distributions!)

3) Open Terminal(Mac/Linux) or Anaconda prompt(Windows) 

4) download PiVR:

   `git clone https://gitlab.com/louislab/PiVR`

5) download the scripts:

    `git clone https://gitlab.com/davidtadres/csh_analysis`

6) cd to the downloaded folder:

    `cd csh_analysis`

7) Create environment for PiVR:

    `conda env create --file=PiVR_environment.yml`

8) Create environment for PiVR:

    `conda env create --file=csh_analysis_env.yml`

9) Activate environment for csh_analysis_env

    `conda activate csh_analysis_env`

10) Run jupyter lab by typing:

    `jupyter lab`

11) In another terminal, activate PiVR environment

    `conda activate PiVR_environment`

12) go to PiVR folder (Do this 2 times as indicated)

    `cd PiVR`

    `cd PiVR` 

13) start PiVR

    `python start_GUI.py`

# OLD INSTRUCTIONS

Should also work, but easier to do the above.

## prepare python environment

1) Install either Miniconda (https://conda.io/miniconda.html) or
Anaconda (https://www.anaconda.com/download/).


2) Install Git (https://git-scm.com/downloads) (already installed on
many Linux distributions!)


3) Open Terminal(Mac/Linux) or Command Prompt(Windows)


4) download the scripts:

    `git clone https://gitlab.com/davidtadres/csh_analysis`


5) create python environment

   `conda create -n csh_analysis_env`


6) Activate the environment:

   Windows:

   `activate csh_analysis_env`

   MacOS:

    `source activate csh_analysis_env`


7) Install the necessary packages:

   `conda install matplotlib -y`

   `conda install pandas -y`

   `conda install -c anaconda jupyter -y`

   `conda install scipy -y`

9) Start jupyter notebook:

   `jupyter notebook`